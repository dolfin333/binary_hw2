import { createElement } from '../helpers/domHelper';
import { fighters } from '../helpers/mockData';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (fighter) {

    const fighterInfo = createElement({
      tagName: 'div',
      className: `fighter_info`,
    });

    for (var key in fighter) {
      if (key !== "source" && key !== "_id") {
        const element = createElement({
          tagName: 'div',
          className: `${key}`,
        });
        element.innerText = `${key[0].toUpperCase() + key.slice(1)}: ${fighter[key]}`;
        fighterInfo.append(element);
      }
    }

    fighterElement.append(createFighterImage(fighter), fighterInfo);
  }
  // todo: show fighter info (image, name, health, etc.)

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
