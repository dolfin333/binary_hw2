import { controls } from '../../constants/controls';

export function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {

    const FighterOne = {
      ...firstFighter,
      block: false,
      CritHitComb: new Set(),
      CritHitComb_time: Date.now() - 10000,
      position: 'left',
      health_bar: document.getElementById("left-fighter-indicator"),
      cur_health: firstFighter.health
    }

    const FighterTwo = {
      ...secondFighter,
      block: false,
      CritHitComb: new Set(),
      CritHitComb_time: Date.now() - 10000,
      position: 'right',
      health_bar: document.getElementById("right-fighter-indicator"),
      cur_health: secondFighter.health
    }

    AddEventListeners();

    function AddEventListeners() {
      document.addEventListener('keydown', KeyDown);
      document.addEventListener('keyup', KeyUp);
    }

    function RemoveEventListeners() {
      document.removeEventListener('keydown', KeyDown);
      document.removeEventListener('keyup', KeyUp);
    }

    function DoDamage(defender, damage) {
      defender.cur_health = defender.cur_health - damage;
      const hb_width = defender.cur_health * 100 / defender.health;
      defender.health_bar.style.width = (hb_width <= 0 ? 0 : hb_width) + '%';
      if (defender.cur_health <= 0) {
        RemoveEventListeners();
        defender.position === "left" ?
          resolve(FighterTwo) :
          resolve(FighterOne);
      }
    }

    function CritHit(code, attacker, defender) {
      attacker.CritHitComb.add(code);
      if (attacker.CritHitComb.size === 3) {
        const date_now = Date.now();
        if (date_now - attacker.CritHitComb_time >= 10000) {
          DoDamage(defender, attacker.attack * 2);
          attacker.CritHitComb_time = date_now;
        }
        attacker.CritHitComb.clear();
      }
    }

    function KeyDown(e) {
      if (!e.repeat) {
        const code = e.code;
        if (code === controls.PlayerOneAttack) {
          if (!FighterOne.block && !FighterTwo.block) {
            DoDamage(FighterTwo, getDamage(FighterOne, FighterTwo))
          }
        }

        if (code === controls.PlayerOneBlock) {
          FighterOne.block = true;
        }

        if (code === controls.PlayerTwoAttack) {
          if (!FighterTwo.block && !FighterOne.block) {
            DoDamage(FighterOne, getDamage(FighterTwo, FighterOne))
          }
        }
        if (code === controls.PlayerTwoBlock) {
          FighterTwo.block = true;
        }

        if (controls.PlayerOneCriticalHitCombination.includes(code)) {
          CritHit(code, FighterOne, FighterTwo);
        }
        if (controls.PlayerTwoCriticalHitCombination.includes(code)) {
          CritHit(code, FighterTwo, FighterOne);
        }
      }
    }

    function KeyUp(e) {
      const code = e.code;
      if (code === controls.PlayerOneBlock) {
        FighterOne.block = false;
      }
      if (code === controls.PlayerTwoBlock) {
        FighterTwo.block = false;
      }
      if (controls.PlayerOneCriticalHitCombination.includes(code)) {
        FighterOne.CritHitComb.delete(code);
      }
      if (controls.PlayerTwoCriticalHitCombination.includes(code)) {
        FighterTwo.CritHitComb.delete(code);
      }
    }

  });
}

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage <= 0 ? 0 : damage;
}

export function getHitPower(fighter) {
  const criticalHitChance = Math.random() + 1;
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  const dodgeChance = Math.random() + 1;
  return fighter.defense * dodgeChance;
}
